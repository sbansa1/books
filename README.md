# myBooks

```bash
.
├── C
│   └── Stephen G. Kochan Programming in C.epub
├── C++
├── DataScience
│   ├── (Advances in Intelligent Systems and Computing 456) Maria Brigida Ferraro, Paolo Giordani, Barbara Vantaggi, Marek Gagolewski, María Ángeles Gil, Przemysław Grzegorzewski, Olgierd Hryniewicz (eds.)-So.pdf
│   ├── Allen B. Downey-Think Stats, 2nd Edition_ Exploratory Data Analysis-O'Reilly Media (2014).pdf
│   ├── Brian Steele, John Chandler, Swarna Reddy-Algorithms for Data Science-Springer (2017).pdf
│   ├── Jeroen Janssens Data Science at the Command Line Facing the Future with Time-Tested Tools.pdf
│   ├── Modern Data Science with R.pdf
│   ├── Ted Dunning, Ellen Friedman-Time Series Databases_ New Ways to Store and Access Data-O'Reilly Media (2014).pdf
│   └── Torgo, Luís Data Mining with R Learning with Case Studies, Second Edition.pdf
├── HFT
│   ├── High_Frequency_Trading_and_Modeling_in_Finance.pdf
│   ├── High-frequency trading - a practical guide to algorithmic strategies and trading systems.pdf
│   ├── High-frequency Trading.pdf
│   └── [Zhaodong_Wang,_Weian_Zheng]_High-Frequency_Tradining_and_Probability_Theory.pdf
├── MySQL
├── Python
│   ├── Automate the Boring Stuff with Python.pdf
│   ├── Daniel Y. Chen-Pandas for Everyone.  Python Data Analysis-Addison-Wesley Professional (2017).pdf
│   ├── Data Science Essentials in Python Collect - Organize - Explore - Predict - Value.pdf
│   ├── Fluent Python - Clear, Concise, and Effective Programming.pdf
│   ├── [Giancarlo_Zaccone]_Python_Parallel_Programming_Co(b-ok.org).pdf
│   ├── HighPerformancePythonfromTrainingatEuroPython2011_v0.2.pdf
│   ├── High Performance Python.pdf
│   ├── Luciano Ramalho-Fluent Python-O'Reilly Media (2015).pdf
│   ├── Michael Heydt-Learning pandas_ Get to grips with pandas - a versatile and high-performance Python library for data manipulation, analysis, and discovery-Packt Publishing (2015).pdf
│   ├── Michael Heydt-Mastering Pandas for Finance-Packt Publishing (2015).pdf
│   ├── [Micha_Gorelick,_Ian_Ozsvald]_High_Performance_Pyt(b-ok.org).pdf
│   ├── Micha Gorelick, Ian Ozsvald-High Performance Python_ Practical Performant Programming for Humans-O'Reilly Media (2014).pdf
│   ├── Python High Performance Programming - Lanaro, Gabriele.pdf
│   ├── Python Programming Advanced.djvu
│   ├── Steven F. Lott-Mastering Object-oriented Python-Packt Publishing (2014).pdf
│   ├── Violent Python.pdf
│   └── Web Scraping with Python Collecting Data from the Modern Web.pdf
├── Quant
│   ├── Algorithmic Trading and DMA.pdf
│   ├── Applied Quantitative Methods for Trading and Investment.pdf
│   ├── Automated Trading with R.pdf
│   ├── Empirical Market Microstructure.pdf
│   ├── Financial Econometrics and Empirical Market Microstructure.pdf
│   ├── Financial Markets and Trading.pdf
│   ├── High-Frequency Trading  A Practical Guide to Algorithmic Strategies and Trading Systems.pdf
│   ├── Howard B. Bandy-Quantitative Trading Systems_ Practical Methods for Design, Testing, and Validation-Blue Owl Press (2007).pdf
│   ├── [Madhavan_A.]_Market_microstructure_A_practitioner_guide.pdf
│   ├── Michael Halls Moore-Advanced Algorithmic Trading (2017).pdf
│   ├── (New Developments in Quantitative Trading and Investment) Christian L. Dunis, Peter W. Middleton, Andreas Karathanasopolous, Konstantinos Theofilatos (eds.)-Artificial Intelligence in Financial Market.pdf
│   ├── Pairs Trading Quantitative Methods and Analysis.pdf
│   ├── Quantitative Analysis of Market Data (1).pdf
│   ├── Quantitative Analysis of Market Data.pdf
│   ├── Quantitative Investment Analysis.pdf
│   ├── Quantitative Investment Analysis, Workbook.pdf
│   ├── Quantitative Methods for Investment Analysis.pdf
│   ├── Statistics and Data Analysis for Financial Engineering
│   │   ├── bonds.R
│   │   ├── bugsfiles.zip
│   │   ├── datasets
│   │   │   ├── AirPassengers.csv
│   │   │   ├── AlphaBeta.csv
│   │   │   ├── berndtInvest.csv
│   │   │   ├── bmw.csv
│   │   │   ├── bmwRet.csv
│   │   │   ├── bondprices.txt
│   │   │   ├── capm2.csv
│   │   │   ├── Capm.csv
│   │   │   ├── CokePepsi.csv
│   │   │   ├── countries.txt
│   │   │   ├── CPI.csv
│   │   │   ├── CPI.dat.csv
│   │   │   ├── CPS1988.csv
│   │   │   ├── CreditCard.csv
│   │   │   ├── cree.csv
│   │   │   ├── CRSPday.csv
│   │   │   ├── CRSPmon.csv
│   │   │   ├── DefaultData.txt
│   │   │   ├── DowJones30.csv
│   │   │   ├── Earnings.csv
│   │   │   ├── equityFunds.csv
│   │   │   ├── EuStockMarket.csv
│   │   │   ├── EuStockMarkets.csv
│   │   │   ├── FamaFrenchDaily.txt
│   │   │   ├── FamaFrench_mon_69_98.txt
│   │   │   ├── FlowData.csv
│   │   │   ├── ford.csv
│   │   │   ├── FourStocks_Daily2013.csv
│   │   │   ├── FrozenJuice.csv
│   │   │   ├── Garch.csv
│   │   │   ├── GPRO.csv
│   │   │   ├── HousePrices.csv
│   │   │   ├── Hstarts.csv
│   │   │   ├── IBM_SP500_04_14_daily_netRtns.csv
│   │   │   ├── Icecream.csv
│   │   │   ├── IncomeUK.csv
│   │   │   ├── IP.csv
│   │   │   ├── IP.dat.csv
│   │   │   ├── Irates.csv
│   │   │   ├── KelvinFlowData.csv
│   │   │   ├── MacroVars.csv
│   │   │   ├── MCD_PriceDaily.csv
│   │   │   ├── midcapD.csv
│   │   │   ├── midcapD.ts.csv
│   │   │   ├── Mishkin.csv
│   │   │   ├── mk.maturity.csv
│   │   │   ├── mk.zero2.csv
│   │   │   ├── msft.csv
│   │   │   ├── nelsonplosser.csv
│   │   │   ├── prog.R
│   │   │   ├── RecentFord.csv
│   │   │   ├── siemens.csv
│   │   │   ├── S&P500.csv
│   │   │   ├── SP500.csv
│   │   │   ├── S&P500_new.csv
│   │   │   ├── Stock_Bond_2004_to_2006.csv
│   │   │   ├── Stock_Bond.csv
│   │   │   ├── Stock_FX_Bond_2004_to_2005.csv
│   │   │   ├── Stock_FX_Bond_2004_to_2006.csv
│   │   │   ├── Stock_FX_Bond.csv
│   │   │   ├── strips_dec95.txt
│   │   │   ├── TbGdpPi.csv
│   │   │   ├── Tbrate.csv
│   │   │   ├── treasury_yields.txt
│   │   │   ├── USMacroG.csv
│   │   │   ├── WeekInt.txt
│   │   │   ├── WeeklyInterest.txt
│   │   │   ├── yields.txt
│   │   │   └── ZeroPrices.txt
│   │   ├── datasets.zip
│   │   ├── EDA.R
│   │   ├── my.R
│   │   ├── returns.R
│   │   ├── R scripts for Statistics and Data Analysis for Financial Engineering with R Examples, 2nd ed..html
│   │   ├── SDAFE2.R
│   │   └── Solutions to Selected R Lab Problems and Exercises Statistics and Data Analysis for Financial Engineering with R Examples, 2nd ed..html
│   ├── Statistics and Data Analysis for Financial Engineering.pdf
│   ├── Statistics and Finance An Introduction.pdf
│   ├── The art and science of technical analysis.pdf
│   ├── Tim Leung, Xin Li-Optimal Mean Reversion Trading_ Mathematical Analysis and Practical Applications-World Scientific Publishing Company (2016).pdf.crdownload
│   ├── Trading and Exchanges Market Microstructure for Practitioners .pdf
│   ├── (Wiley Finance) Wesley R. Gray, Jack R. Vogel-Quantitative Momentum_ A Practitioner’s Guide to Building a Momentum-Based Stock Selection System-Wiley (2016).pdf
│   └── (Wiley Trading) Ernest P. Chan-Machine Trading_ Deploying Computer Algorithms to Conquer the Markets-Wiley (2017).pdf
├── R
│   ├── Data Analysis with R.pdf
│   ├── efficient-master.zip
│   ├── Efficient_R_Programming_A_Practical_Guide_to_Smarter_Programming.pdf
│   ├── Efficient R Programming.pdf
│   ├── [Hadley_Wickham_(auth.)]_ggplot2_Elegant_Graphics(b-ok.org).pdf
│   ├── Hadley Wickham, Garrett Grolemund-R for Data Science_ Import, Tidy, Transform, Visualize, and Model Data-O’Reilly Media (2017).pdf
│   ├── Learning_R_Programming_－_Become_an_efficient_data_scientist_with_R.pdf
│   ├── N.D Lewis-Deep Learning Made Easy with R_ A Gentle Introduction For Data Science-CreateSpace Independent Publishing Platform (2016).pdf
│   ├── Raja B. Koushik, Sharan Kumar Ravindran-R Data Science Essentials-Packt (2016).pdf
│   ├── Thomas Mailund (auth.)-Advanced Object-Oriented Programming in R_ Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
│   ├── Thomas Mailund (auth.)-Metaprogramming in R_ Advanced Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
│   ├── Thomas Mailund-Beginning Data Science in R_ Data Analysis, Visualization, and Modelling for the Data Scientist-Apress (2017).pdf
│   └── Thomas Mailund-Functional Programming in R. Advanced Statistical Programming for Data Science, Analysis and Finance-Apress (2017).pdf
├── Social
│   └── 万历十五年.mobi
├── Stat
│   ├── Advanced Linear Modeling.pdf
│   ├── All Figures.zip
│   ├── All Labs.txt
│   ├── An Elementary Introduction to Statistical Learning Theory.pdf
│   ├── An Introduction to Statistical Learning.pdf
│   ├── An Introduction to Statistical Learning with Applications in R.pdf
│   ├── Applying Regression and Correlation.pdf
│   ├── (Chapman & Hall_CRC Texts in Statistical Science) Norman Matloff-Statistical Regression and Classification_ From Linear Models to Machine Learning-Chapman and Hall_CRC (2017).pdf
│   ├── Elements of Statistics for the Life and Social Sciences.pdf
│   ├── Foundations of Statistical Algorithms.pdf
│   ├── Introduction to Statistical Machine Learning.pdf
│   ├── Plane Answers to Complex Questions The Theory of Linear Models.pdf
│   ├── Statistical Analysis and Data Display.pdf
│   └── Statistics_for_Finance.pdf
└── Unix
    ├── Anoop Chaturvedi, B.L. Rai-Unix and Shell Programming-Laxmi Publications (2017).pdf
    ├── B. Kernighan, R. Pike UNIX - Programming Environment .pdf
    ├── (Developer’s Library) Stephen G. Kochan, Patrick Wood-Shell Programming in Unix, Linux and OS X-Addison-Wesley Professional (2016).pdf
    ├── W. Richard Stevens, Stephen A. Rago Advanced Programming in the UNIX Environment.pdf
    └── W. Richard Stevens, Stephen A. Rago Advanced Programming in the UNIX R Environment.pdf
```
